import Exceptions.CustomException;

public class Exercici02App {

	public static void main(String[] args) {
		// Programa treu per pantalla una excepci� customitzada

		try {
			System.out.println("Mensaje mostrado por pantalla");
			CustomException excepcio = new CustomException(1);
			System.out.println(excepcio);
		} finally {
			System.out.println("Programa terminado");
		}
	}

}
