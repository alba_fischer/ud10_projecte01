package Exceptions;

public class CustomException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Atributs 
	int tipusError;
	
	//Constructor per defecte
	public CustomException() {
	}
	
	//Constrctor amb atribut
	public CustomException (int error) {
		this.tipusError = error;
	}

	@Override
	public String getMessage() {
		//M�tode per mostrar el missatge customitzat
		String mensaje = " ";
		switch (tipusError) {
			case 1:
				mensaje = "Excepcion capturada con mensaje: Esto es un objeto Exception";
				break;
		}
		return mensaje;
		
	}
	
	
	
}
