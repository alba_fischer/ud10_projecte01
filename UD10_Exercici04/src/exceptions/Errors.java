package exceptions;

import java.util.InputMismatchException;

import views.InteraccioUsuari;

public class Errors{
	
	private InteraccioUsuari interaccio = new InteraccioUsuari();
	public int comprovarNombre () {
		//Controlem que s'introdueixin nombres enters
		int num1 = 0;
		boolean control = true;
		do {
			control = true;
			try {
				num1 = interaccio.nombre();
			}catch(InputMismatchException e){
				System.out.println("Els car�cters introdu�ts s�n incorrectes. Han de ser n�meros enters!");
				control = false;
			}
		
		}while(control == false);
	
	return num1;
	}
	
	public int[] comprovarNombres () {
		//Controlem que s'introdueixin nombres enters
		int[] variables = null;
		boolean control;
		do {
			control = true;
			try {
				variables = interaccio.nombres();
			
			}catch(InputMismatchException e){
				System.out.println("Els car�cters introdu�ts s�n incorrectes. Han de ser n�meros enters!");
				control = false;
			}
		
		}while(control == false);
	
	return variables;
	}
	
	public int comprovarOperacio () {
		//Controlem que s'introdueixin nombres enters  i dins d'un interval per poder escollir b� l'operaci�
		boolean control;
		int operacio = 0;
		do {
			control = true;
			try {
				operacio = interaccio.signe();
				if (operacio < 1 || operacio > 7) {
					control = false;
					System.out.println("El n�mero de l'operaci� ha de pert�nyer a l'interval [1,7].");
				}
				
			}catch(InputMismatchException e){
				System.out.println("Els car�cters introdu�ts s�n incorrectes. Han de ser n�meros enters!");
				control = false;
			}
		}while(control == false);
		
	return operacio;
	}
	
	public static String dividir (int a, int b) {
		//Controlem que no hi hagi una divisi� per 0.
		String resultats = " ";
		
		try { 
			int resultat = a/b;
			
		}catch(ArithmeticException e) {
			resultats = "Math Error! No es pot dividir entre 0.";
			
		}
		return resultats;	
		
	}
	
	
}
