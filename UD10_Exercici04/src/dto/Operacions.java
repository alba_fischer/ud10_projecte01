package dto;

import exceptions.Errors;

public class Operacions {
	/*Fem un programa que permeti fer c�lculs simples (suma, resta, multiplicaci�, divisi�, pot�ncia,
	 * arrel quadrada, arrel c�bica)
	 */
	
	public void calculs(int num1, int num2, int signe) {
		switch(signe) {
			case 1:
				System.out.println("Suma " + num1 + " + " + num2 + " = " + (num1+num2));
				break;
			case 2:
				System.out.println("Resta " + num1 + " - " + num2 + " = " + (num1-num2));
				break;
			case 3:
				System.out.println("Multiplicaci� " + num1 + " * " + num2 + " = " + (num1*num2));
				break;
			case 4:
				System.out.println("Pot�ncia " + num1 + " ^ " + num2 + " = " + Math.pow(num1, num2));
				
				break;
			case 7: 
				//En cas de que el num2 sigui 0, el resltat de la divisi� no existeix i per aix� ho hem de passar per una excepci�.
				if (num2 == 0) {
					System.out.println("Divisi� " + num1 + " / " + num2 + " = " + Errors.dividir(num1, num2));	
				}else {
					System.out.println("Divisi� " + num1 + " / " + num2 + " = " + num1/num2);
				}
				break;
		}
	}
			
	public void calculs(int num1, int signe) {		
			
		switch (signe) {
			case 5:
				/*En aquest cas, si el nombre introdu�t �s negatiu, el resultat ser� NaN, ja que no existeixen els resultats de les arrels quadrades negatives
				 * Tot i donar NaN el programa igual s'executa, per aix� no s'ha fet cap excepci�.
				 */
				System.out.println("Arrel quadrada de " + num1 + " = " + Math.sqrt(num1));
				break;
				
			case 6:
				System.out.println("Arrel c�bica de " + num1 + " = " + Math.cbrt(num1));
				break;	
			
	}
	}
}

