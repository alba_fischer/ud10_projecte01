import dto.Operacions;
import exceptions.Errors;

public class Exercici04App {

	public static void main(String[] args) {
		/*Fem un programa que realitzi diverses operacions simples.
		 * Ha d'estar preparat per gestionar els possibles errors de c�lcul.
		 */
		Errors error = new Errors();
		Operacions operacions =  new Operacions();
		int signe = error.comprovarOperacio();
		if (signe == 1 || signe == 2 || signe == 3 || signe == 4 || signe == 7) {
			int [] variable = error.comprovarNombres ();
			operacions.calculs (variable[0],variable[1], signe);
		} else if (signe == 5 || signe == 6) {
			operacions.calculs (error.comprovarNombre (), signe);
		}	
	}

}
