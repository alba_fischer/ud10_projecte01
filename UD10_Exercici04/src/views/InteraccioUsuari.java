package views;

import java.util.Scanner;

public class InteraccioUsuari {
	//L'usuari introdueix els nombres i l'operaci� a realitzar per teclat
	
	public int[] nombres() {
		//Introdu�m dos nombres 
		Scanner scan = new Scanner(System.in);
		int num1 = 0, num2 = 0;
		System.out.println("Introdueix un nombre enter: ");
		num1 = scan.nextInt();	
		System.out.println("Introdueix un altre nombre enter: ");
		num2 = scan.nextInt();
		
		int [] variables = {num1,num2};
		return variables;
	}
	
	public int nombre() {
		//Introdu�m nom�s un nombre en el cas de les arrels
		Scanner scan = new Scanner(System.in);
		int num1 = 0;
		System.out.println("Introdueix un nombre enter:");
		num1 = scan.nextInt();	
		return num1;
	}
	
	public int  signe(){
		//Operaci� a realitzar
		Scanner scan = new Scanner(System.in);
		int operacio;
		System.out.println("Introdueix el nombre de la operaci� que vulguis fer: 1-suma, 2-resta, 3-multiplicaci�, 4-potencia, 5-arrel quadrada, 6-arrel cubica, 7-divisi�:");
		operacio = scan.nextInt();	
		
		return operacio;
	}

}