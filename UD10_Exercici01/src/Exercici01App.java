import dto.EndevinarNombre;
import exceptions.Excepcions;

public class Exercici01App {

	public static void main(String[] args) {
		//L'ordinador genera un nombre entre 1 i 500, i l'usuari l'ha d'endevinar.
		EndevinarNombre nombre = new EndevinarNombre();
		nombre.numAleatori();
		boolean comprova = false;
		int intent = 0;
		int [] variables;
		do { 
			variables = Excepcions.excepcio(intent);
			comprova = nombre.comprovacio(variables[0]);
			intent = variables[1];
		}while(comprova == false);
		System.out.println("Enhorabona, ho has endevinat amb " + intent + " intents!");

	}
}