package dto;

import java.util.Random;



public class EndevinarNombre {
	private int num;
	Random random = new Random(); 
	
	//Generem un nombre aleatori entre 1 i 500
	public void numAleatori() {
		this.num = random.nextInt(500)+1;
	}
	
	/*Cada vegada que l'usuari introdueix un valor, l'ordinador li ha de dir si el n�mero que ha d'endevinar 
	 * �s major o menos que el que ha introdu�t.
	 */
	public boolean comprovacio (int num) {
		boolean encertat = false;
		
		if (this.num == num){
			encertat = true;
		}
		else if (this.num > num){
			System.out.println("El nombre introdu�t �s menor al que has d'endevinar. ");			
		}
		else if (this.num < num){
			System.out.println("El nombre introdu�t �s major al que has d'endevinar. ");
		}
		return encertat;
	}
	
}
