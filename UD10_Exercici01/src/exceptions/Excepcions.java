package exceptions;

import java.util.InputMismatchException;


import views.InterficieVisual;

public class Excepcions {
	/*Si l'usuari introdueix algo que no �s un n�mero s'ha d'indicar per pantalla
	 *i contar-ho com a intent. A m�s a m�s, l'usuari nom�s ha d'introdu�r valors de
	 *l'1 al 500.  
	 */
	public static int [] excepcio(int intents){
		boolean control;
		
		int num = 0;
		do {
			control = true;
			try{
				num = InterficieVisual.intent();
				intents = intents + 1;
				if (num < 1 || num > 500) {
					control = false;
					System.out.println("El n�mero ha de pert�nyer a l'interval [1,500].");
					
				}
				
			}catch(InputMismatchException e){
				control = false;
				System.out.println("El car�cter introdu�t �s incorrecte. Perfavor introdueix un nombre enter.");
				intents = intents + 1;
			
		}
		}while(control == false);
		int [] variables = {num,intents};
		return variables;
			
	}
	
}

