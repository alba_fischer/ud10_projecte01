package views;

import java.util.Scanner;

public class InterficieVisual {
	
	//L'usuari introdueix els nombres per teclat
	public static int intent() {
		int num = 0;
		System.out.println("Introdueix un nombre de l'1 al 500: ");
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		num = scan.nextInt();	
		
		return num;
	}
	
}
