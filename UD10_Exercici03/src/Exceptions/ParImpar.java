package Exceptions;

public class ParImpar extends Exception {
	//Atributs
	int tipusExcepcio;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Constructor per defecte
	public ParImpar() {
	}
	
	//Constructor amb atributs
	public ParImpar(int num) {
		if (num % 2 == 0) {
			this.tipusExcepcio = 1;
		}else {
			this.tipusExcepcio = 2;
			}
	}
	
	
	public String getMessage() {
		String mensaje = " ";
		//M�tode per mostrar el missatge de si el nombre �s par o impar
		switch (tipusExcepcio) {
			case 1:
				mensaje	= "El n�mero �s par.";
				break;
				
			case 2:
				mensaje	= "El n�mero �s impar.";
				break;
			}
		
		return mensaje;	
			
	}
}
