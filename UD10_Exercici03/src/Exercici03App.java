import java.util.Random;

import Exceptions.ParImpar;

public class Exercici03App {

	public static void main(String[] args) {
		// Generem un nombre aleat�ri i indiquem si �s par o impar
		try {
			System.out.println("Generando n�mero aleatorio...");
			Random random = new Random();
			int num = random.nextInt(1000);
			System.out.println("El n�mero aleatorio es: " + num);
			throw new ParImpar(num);
		}catch(ParImpar e) {
			System.out.println(e.getMessage());
		}
	}

}
