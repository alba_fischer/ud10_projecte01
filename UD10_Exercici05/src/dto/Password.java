package dto;

import java.util.Random;

public class Password {
	//Atributs
	
	private int longitud;
	private String contrasenya;
	
	//CONSTRUCTORS
	
	//Constructor per defecte
	public Password() {
		this.longitud = 8;
		this.contrasenya = "";
	}
	
	//Constructor amb longitud per teclat i contrasenya aleat�ria
	public Password(int longitud) {
		this.longitud = longitud;		
		this.contrasenya = generarPassword(this.longitud);
	}
	
	private static final String ALFABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	public static String generarPassword (int longitud)  {
		//Generem una contrasenya aleat�ria
	    Random random = new Random();
	    StringBuilder contra = new StringBuilder(longitud);

	    for (int i = 0; i < longitud; i++) {
	        contra.append(ALFABET.charAt(random.nextInt(ALFABET.length())));
	    }

	    return contra.toString();
	    
	}
	
	public static boolean esFuerte(String password) {
		/*M�tode que retorna un boolean si �s fort o no, per a que sigui fort ha de tindre m�s de dues maj�scules,
		 * m�s d'una min�scula i m�s de 5 n�meros.
		 */
		
		int majuscules = 0;
		int minuscules = 0;
		int numeros = 0;
		boolean fort = false;
		String majus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String minus = "abcdefghijklmnopqrstuvwxyz";
		String nums = "1234567890";
		for (int i = 0; i < password.length(); i++) {
			for (int j = 0; j < majus.length(); j++) {
				if (password.charAt(i) == majus.charAt(j)) {
					majuscules = majuscules + 1;
				}else if (password.charAt(i) == minus.charAt(j)) {
					minuscules = minuscules + 1;
				}
			}
		}
			
		for (int i = 0; i < password.length(); i++) {
			for (int j = 0; j < nums.length(); j++) {
				if (password.charAt(i) == nums.charAt(j)) {
					numeros = numeros + 1;
				}
			}
		}
		if (majuscules>2 && minuscules>1 && numeros>5) {
			fort = true;
		}
		return fort;
	}

	// To string
	@Override
	public String toString() {
		return "Password [longitud=" + longitud + ", contrasenya=" + contrasenya + "]";
	}

	//Getters and setters
	public int getLongitud() {
		return longitud;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	public String getContrasenya() {
		return contrasenya;
	}

	public void setContrasenya(String contrasenya) {
		this.contrasenya = contrasenya;
	}
	
	
	
	
}
