package exceptions;

import java.util.InputMismatchException;

import views.Interficie;


public class Excepcions {
	//Controlem que els valors de la longitud i de la mida de l'array introdu�ts per teclat siguin nombres enters
	public static int excepcio(){
		boolean control;
		Interficie inter = new Interficie();
		int num = 0;
		do {
			control = true;
			try{
				num = inter.longi();
				
			}catch(InputMismatchException e){
				control = false;
				System.out.println("El car�cter introdu�t �s incorrecte. Perfavor introdueix un nombre enter.");
				
			}
		}while(control == false);
		
		return num;
			
	}
	
	public static int excepcio1(){
		boolean control;
		Interficie inter = new Interficie();
		int num = 0;
		do {
			control = true;
			try{
				num = inter.midaArray();
				
			}catch(InputMismatchException e){
				control = false;
				System.out.println("El car�cter introdu�t �s incorrecte. Perfavor introdueix un nombre enter.");
				
			}
		}while(control == false);
		
		return num;
			
	}
	
}
